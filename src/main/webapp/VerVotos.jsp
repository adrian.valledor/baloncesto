<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="Modelos.Jugador"%>
<%@ page import="Modelos.ModeloDatos"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
      crossorigin="anonymous"
    />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Tabla de Votaciones</title>
  </head>
  <body>
    <h1>Tabla de Votaciones Liga ACB</h1>

  <% 
    ArrayList<Jugador> listaJugadores = ModeloDatos.getJugadores();

  %>

    <table class="table table-hover" name="tablaVotaciones">
      <caption>Tabla de Votaciones</caption>
      <thead class="thead-light">
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Nombre jugador</th>
          <th scope="col">Votos</th>
        </tr>
      </thead>
      <tbody>

        <% 
        for(Jugador jugador : listaJugadores) { %>
          <tr>
            <td><%= jugador.getId() %></td>
            <td><%= jugador.getNombre() %></td>
            <td><%= jugador.getVotos() %></td>
          </tr>
         <%
        }     
           %>
          
      </tbody>
    </table>
    <br />
    <br />
    <a href="index.html"> Ir al comienzo</a>
  </body>
</html>
