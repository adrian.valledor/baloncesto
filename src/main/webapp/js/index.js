function ponerVotosCero() {
  $.ajax({
    url: 'PonerACero',
    type: 'POST',
    data: {},
    async: false,
    success: function () {
      alert('Votos puestos a cero')
    },
    error: function (jqXHR, textStatus, errorThrown) {
      alert('Error al poner los votos a cero')
    },
  })
}

function verVotos() {
  window.location.href = 'VerVotos.jsp'
}
