
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import Modelos.ModeloDatos;

@WebServlet("/PonerACero")
public class PonerACero extends HttpServlet {

  public PonerACero() {
    super();
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();

    try {
      boolean correcto = ModeloDatos.ponerCero();
      if (correcto) {
        out.print("Los votos se han puesto a cero");
      } else {
        out.print("No se ha podido poner a cero los votos");
      }
    } catch (Exception e) {
      out.print("Error" + e);
    } finally {
      out.close();
    }

  }
}
