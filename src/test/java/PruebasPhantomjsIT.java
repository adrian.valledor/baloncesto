import java.util.ArrayList;
import java.util.List;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PruebasPhantomjsIT {
  private static WebDriver driver = null;

  /*
   * @Test
   * void tituloIndexTest() {
   * DesiredCapabilities caps = new DesiredCapabilities();
   * caps.setJavascriptEnabled(true);
   * caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
   * "/usr/bin/phantomjs");
   * caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
   * new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
   * driver = new PhantomJSDriver(caps);
   * driver.navigate().to("http://localhost:8080/Baloncesto/");
   * assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(),
   * "El titulo no es correcto");
   * System.out.println(driver.getTitle());
   * driver.close();
   * driver.quit();
   * }
   */

  @Test
  void ponerVotosCeroTest() {
    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setJavascriptEnabled(true);
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
        new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
    driver = new PhantomJSDriver(caps);
    driver.navigate().to("http://localhost:8080/Baloncesto/");

    driver.findElement(By.name("B3")).click();
    driver.findElement(By.name("B4")).click();

    WebElement votaciones = driver.findElement(By.name("tablaVotaciones"));
    ArrayList<WebElement> col = new ArrayList<>(votaciones.findElements(By.tagName("tr")));
    col.remove(0);

    ArrayList<WebElement> listaVotos = new ArrayList<>();

    for (int i = 0; i < col.size(); i++) {
      List<WebElement> campoVoto = col.get(i).findElements(By.tagName("td"));
      listaVotos.add(campoVoto.get(2));
    }

    boolean ok = true;
    int cuentaVotos = 0;

    while (ok && cuentaVotos < listaVotos.size()) {
      if (!listaVotos.get(cuentaVotos).getText().trim().equals("0")) {
        ok = false;
      }
      cuentaVotos++;
    }

    assertEquals(true, ok, "No se puede poner los votos a cero");

    driver.close();
    driver.quit();
  }

  @Test
  void votarOtroJugadorTest() {
    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setJavascriptEnabled(true);
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
        new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
    driver = new PhantomJSDriver(caps);
    driver.navigate().to("http://localhost:8080/Baloncesto/");

    driver.findElement(By.id("otroJugador")).click();
    String jugador = "Tavares";
    driver.findElement(By.name("txtOtros")).sendKeys(jugador);
    driver.findElement(By.name("B1")).click();
    driver.findElement(By.name("volver")).click();
    driver.findElement(By.name("B4")).click();

    WebElement votaciones = driver.findElement(By.name("tablaVotaciones"));
    ArrayList<WebElement> col = new ArrayList<>(votaciones.findElements(By.tagName("tr")));
    col.remove(0);

    boolean ok = false;
    int cuentaVotos = 0;

    while (!ok && cuentaVotos < col.size()) {
      List<WebElement> fila = col.get(cuentaVotos).findElements(By.tagName("td"));
      if (fila.get(1).getText().trim().equals(jugador) && fila.get(2).getText().trim().equals("1")) {
        ok = true;
      }
      cuentaVotos++;
    }

    assertEquals(true, ok, "No se ha votado a otro jugador");

    driver.close();
    driver.quit();
  }
}