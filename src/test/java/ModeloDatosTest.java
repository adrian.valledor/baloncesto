import org.junit.jupiter.api.Test;

import Modelos.ModeloDatos;

import java.io.FileInputStream;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

public class ModeloDatosTest extends DBTestCase {

  String dbHost = System.getenv().get("DATABASE_HOST");
  String dbPort = System.getenv().get("DATABASE_PORT");
  String dbName = System.getenv().get("DATABASE_NAME");
  String dbUser = System.getenv().get("DATABASE_USER");
  String dbPass = System.getenv().get("DATABASE_PASS");
  String url = dbHost + ":" + dbPort + "/" + dbName;

  public ModeloDatosTest(String name) {
    super(name);
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "com.mysql.cj.jdbc.Driver");
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, url);
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, dbUser);
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, dbPass);
  }

  @Override
  protected IDataSet getDataSet() throws Exception {
    return new FlatXmlDataSetBuilder().build(new FileInputStream("data.xml"));
  }

  @Override
  protected DatabaseOperation getSetUpOperation() throws Exception {
    return DatabaseOperation.REFRESH;
  }

  @Override
  protected DatabaseOperation getTearDownOperation() throws Exception {
    return DatabaseOperation.NONE;
  }

  @Test
  public void testExisteJugador() {
    System.out.println("Prueba de existeJugador");
    String nombre = "Rudy";
    ModeloDatos instance = new ModeloDatos();
    boolean expResult = true;
    boolean result = instance.existeJugador(nombre);
    assertEquals(expResult, result);
    // fail("Fallo forzado.");
  }

  @Test
  public void testActualizarJugador() throws Exception {
    System.out.println("Prueba de actualizarJugador");
    ModeloDatos instance = new ModeloDatos();
    instance.actualizarJugador("Rudy");

    IDataSet expds = new FlatXmlDataSetBuilder().build(new FileInputStream("dataExpected.xml"));
    ITable resultadoEsperado = expds.getTable("Jugadores");
    String valorEsperado = resultadoEsperado.getValue(2, "votos").toString();

    IDatabaseConnection connection = getConnection();
    IDataSet databaseDataSet = connection.createDataSet();
    ITable resultadoActual = databaseDataSet.getTable("Jugadores");
    String valorActual = resultadoActual.getValue(2, "votos").toString();

    assertEquals(valorEsperado, valorActual);

  }

}
